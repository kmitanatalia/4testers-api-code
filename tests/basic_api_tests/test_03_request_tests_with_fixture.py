import requests
import pytest


@pytest.fixture(scope='module')
def my_ip():
    url = "https://api.ipify.org/?format=json"
    response = requests.get(url)
    return response


def test_status_code(my_ip):
    assert my_ip.status_code == 200


def test_response_has_a_body(my_ip):
    data = my_ip.json()
    assert data is not None


def test_is_json(my_ip):
    assert my_ip.headers["Content-type"] == "application/json"
