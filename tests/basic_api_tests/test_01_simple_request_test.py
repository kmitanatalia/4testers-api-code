import requests


def test_my_ip():
    response = requests.get("https://api.ipify.org/?format=json")
    assert response.status_code == 200
    data = response.json()
    assert data is not None
    assert response.headers["Content-type"] == "application/json"
    assert data['ip'] is not None
