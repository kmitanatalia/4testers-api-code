import requests


def test_user_create_fails_without_a_token():
    url = "https://gorest.co.in/public/v2/users"
    response = requests.post(url, '{}')
    assert response.status_code == 401
    assert response.reason == "Unauthorized"
