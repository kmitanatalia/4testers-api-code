from faker import Faker
import requests
from random import choice


def test_user_create_pass_with_valid_token():
    # step 1
    url = "https://gorest.co.in/public/v2/users"
    # step 2
    headers = {
        "Authorization": "Bearer edb9ccc32571593f5c26c2340a6bb5974c855147dc1024c5e9b7838ae56f6739"
    }
    # step 3
    user_data = {
        "name": Faker().name(),
        "gender": choice(["male", "female"]),
        "email": Faker().email(),
        "status": "active"
    }
    # steps 5 - 8
    response_post = requests.post(url, data=user_data, headers=headers)
    assert response_post.status_code == 201
    assert response_post.reason == "Created"

    user_id = response_post.json()['id']
    url_get = url + "/" + str(user_id)
    response_get = requests.get(url_get, headers=headers)
    assert response_get.status_code == 200
    assert response_get.reason == "OK"
    received_data = response_get.json()

    assert user_data['name'] == received_data['name']
    assert user_data['gender'] == received_data['gender']
    assert user_data['email'] == received_data['email']
    assert user_data['status'] == received_data['status']
