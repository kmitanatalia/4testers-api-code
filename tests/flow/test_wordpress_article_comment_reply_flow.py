import time
from base64 import b64encode
import requests
import pytest
import lorem

# Editor information

editor_username = 'Editor'
editor_password = 'HWZg hZIP jEfK XCDE V9WM PQ3t'
editor_email = 'editor@example.com'
editor_token = b64encode(f"{editor_username}:{editor_password}".encode('utf-8')).decode("ascii")

# Commenter information


commenter_username = 'Commenter'
commenter_password = 'SXlx hpon SR7k issV W2in zdTb'
commenter_email = 'commenter@example.com'
commenter_token = b64encode(f"{commenter_username}:{commenter_password}".encode('utf-8')).decode("ascii")


# Blog information


blog_url = 'https://gaworski.net'
posts_endpoint_url = blog_url + "/wp-json/wp/v2/posts"
comments_endpoint_url = blog_url + "/wp-json/wp/v2/comments"


# Fixtures set


@pytest.fixture(scope='module')
def article():
    timestamp = int(time.time())
    article = {
        "article_creation_date": timestamp,
        "article_title": "This is a new article for the API flow tests, Natalia " + str(timestamp),
        "article_subtitle": lorem.sentence(),
        "article_text": lorem.paragraph()
    }
    return article


@pytest.fixture(scope="module")
def commenter_comment():
    comment = {
        "author_name": "commenter",
        "author_email": commenter_email,
        "content": "This is a new comment for the commenters article."
    }
    return comment


@pytest.fixture(scope="module")
def editor_comment():
    comment = {
        "author_name": "editor",
        "author_email": editor_email,
        "content": "This is a new comment for the editors article."
    }
    return comment


@pytest.fixture(scope='module')
def editor_header():
    headers = {
        "Content-Type": "application/json",
        "Authorization": "Basic " + editor_token
    }
    return headers


@pytest.fixture(scope='module')
def commenter_header():
    headers = {
        "Content-Type": "application/json",
        "Authorization": "Basic " + commenter_token
    }
    return headers


@pytest.fixture(scope='module')
def posted_article(article, editor_header):
    payload = {
        "title": article["article_title"],
        "excerpt": article["article_subtitle"],
        "content": article["article_text"],
        "status": "publish"
    }
    print(posts_endpoint_url)
    response = requests.post(url=posts_endpoint_url, headers=editor_header, json=payload)
    return response


@pytest.fixture(scope="module")
def posted_comment(commenter_comment, commenter_header, posted_article):
    parent_article_id = posted_article.json()["id"]
    payload = {
        "author_name": commenter_comment["author_name"],
        "author_email": commenter_comment["author_email"],
        "content": commenter_comment["content"],
        "status": "publish",
        "post": parent_article_id
    }
    response = requests.post(url=comments_endpoint_url, headers=commenter_header, json=payload)
    return response


@pytest.fixture(scope="module")
def answer_editor_comment(posted_comment, editor_header, editor_comment, posted_article):
    parent_comment_id = posted_comment.json()["id"]
    parent_article = posted_article.json()["id"]
    payload = {
        "author_name": editor_comment["author_name"],
        "author_email": editor_comment["author_email"],
        "content": editor_comment["content"],
        "status": "publish",
        "post": parent_article,
        "parent": parent_comment_id
    }
    response = requests.post(url=comments_endpoint_url, headers=editor_header, json=payload)
    return response


# Test 1) Add a new article as an editor user. VERIFY: Success of the CREATE operation


def test_new_post_is_successfully_created(posted_article):
    assert posted_article.status_code == 201
    assert posted_article.reason == "Created"


# VERIFY: Authorship of the article


def test_author_verification_test(article, posted_article):
    wordpress_post_id = posted_article.json()["id"]
    wordpress_post_url = f'{posts_endpoint_url}/{wordpress_post_id}'
    published_article = requests.get(url=wordpress_post_url)
    assert published_article.status_code == 200
    assert published_article.reason == "OK"
    wordpress_post_data = published_article.json()
    print("Article Data:", wordpress_post_data)
    assert wordpress_post_data["title"]["rendered"] == article["article_title"]
    assert wordpress_post_data["excerpt"]["rendered"] == f'<p>{article["article_subtitle"]}</p>\n'
    assert wordpress_post_data["content"]["rendered"] == f'<p>{article["article_text"]}</p>\n'
    assert wordpress_post_data["status"] == 'publish'
    assert wordpress_post_data["author"] == 2


# Test 2) Add a comment to the article added in step 1 as a commenter. VERIFY: Success of the CREATE operation.


def test_create_comment_by_commenter(posted_comment, posted_article):
    assert posted_comment.status_code == 201
    assert posted_comment.reason == "Created"


# VERIFY: Relationship between the post and the comment.


def test_verify_the_relationship_between_post_and_comment(posted_comment, posted_article):
    assert posted_comment.json()["post"] == posted_article.json()["id"]


# VERIFY: Authorship of the comment.


def test_author_verification_test_of_new_comment(posted_comment, commenter_comment):
    wordpress_comment_url = f'{comments_endpoint_url}/{posted_comment.json()["id"]}'
    published_comment = requests.get(wordpress_comment_url)
    wordpress_comment_data = published_comment.json()
    assert published_comment.status_code == 200
    assert published_comment.reason == "OK"
    print("Comment Data:", wordpress_comment_data)
    assert wordpress_comment_data["author_name"] == commenter_comment["author_name"]
    assert wordpress_comment_data["content"]["rendered"] == f'<p>{commenter_comment["content"]}</p>\n'
    assert wordpress_comment_data["author"] == posted_comment.json()["author"]


# Test 3) Add a reply to the comment added in step 2 as an editor. VERIFY: Success of the CREATE operation.


def test_create_editor_answer(answer_editor_comment):
    assert answer_editor_comment.status_code == 201
    assert answer_editor_comment.reason == "Created"


# VERIFY: Relationship between the comment and the reply.


def test_verify_relationship_between_comment_and_reply(answer_editor_comment, posted_comment):
    assert answer_editor_comment.json()["parent"] == posted_comment.json()["id"]


# VERIFY: Relationship between the reply and the article.


def test_verify_relationship_between_answer_and_article(answer_editor_comment, posted_article):
    assert answer_editor_comment.json()["post"] == posted_article.json()["id"]


# VERIFY: Authorship of the reply.


def test_verify_author_of_comment_answer(editor_comment, answer_editor_comment):
    assert answer_editor_comment.json()["author_name"] == editor_comment["author_name"]
